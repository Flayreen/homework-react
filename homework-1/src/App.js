import React, { useState } from 'react';
import Button from "./components/Button";
import Modal from "./components/Modal";
import {buttonsStyle} from "./styles/style";

function App() {
    // Стилізація JSS
    const style = buttonsStyle();

    // Хуки станів модалок
    const [showModal1, setShowModal1] = useState(false);
    const [showModal2, setShowModal2] = useState(false);
    const openModal1 = () => {
        setShowModal1(!showModal1);
    };
    const openModal2 = () => {
        setShowModal2(!showModal2);
    };

    return (
      <div>
          {/* Кнопки */}
          <div className={style.container}>
              <Button
                  backgroundColor="blue"
                  text="Open first modal"
                  fn={openModal1}/>
              <Button
                  backgroundColor="black"
                  text="Open second modal"
                  fn={openModal2}/>
          </div>

          {/* Модалки */}
          {showModal1 && (
              <Modal
                  header="Title 1"
                  closeButton={true}
                  text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
                  actions={[
                      () => <Button backgroundColor="green" text="OK" fn={openModal1}/>,
                      () => <Button backgroundColor="gray" text="Cancel" fn={openModal1}/>
                  ]}
                  closeFn={openModal1}
          />)}
          {showModal2 && (
              <Modal
                  header="Title 2"
                  closeButton={false}
                  text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
                  actions={[
                      () => <Button backgroundColor="orange" text="IDK" fn={openModal2}/>,
                      () => <Button backgroundColor="red" text="Remove" fn={openModal2}/>
                  ]}
                  closeFn={openModal2}
              />)}
      </div>
    );
}

export default App;
