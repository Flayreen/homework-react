import React from 'react';
import {modalStyle} from "../styles/style";
import closeIcon from "../close.svg"

function Modal({header, closeButton, text, actions, closeFn}) {
    const modal = modalStyle();

    return (
        <div onClick={closeFn} className={modal.darkBackground}>
            <div onClick={(event) => event.stopPropagation()} className={modal.container}>
                <div className={modal.headerBlock}>
                    <h1 style={{fontSize: "24px", fontWeight: "700",}}>{header}</h1>
                    { closeButton && (
                        <img
                            src={closeIcon}
                            alt="icon-close"
                            onClick={closeFn}
                        />)}
                </div>
                <p>{text}</p>
                { Boolean(actions.length) && (
                    <div className={modal.blockButtons}>
                        {actions.map((ActionButton, index) => (
                            <ActionButton key={index}/>
                        ))}
                    </div>
                )}
            </div>
        </div>
    );
}

export default Modal;