import React from 'react';
import {buttonsStyle} from "../styles/style";

function Button({text, backgroundColor, fn}) {
    const style = buttonsStyle();

    return (
        <button onClick={fn} className={style.button} style={{backgroundColor}}>
            {text}
        </button>
    );
}

export default Button;