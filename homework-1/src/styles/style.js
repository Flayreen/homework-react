import { createUseStyles } from 'react-jss';

const buttons = {
    container: {
        display: "flex",
        width: "100vw",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center",
        gap: "16px",
    },

    button: {
        width: "150px",
        height: "40px",
        border: "none",
        borderRadius: "8px",
        color: "white",
        fontSize: "14px",

        "&:hover": {
            opacity: "0.8",
            cursor: "pointer",
        }
    }
}

const modal = {
    darkBackground: {
        position: "absolute",
        top: "0",
        left: "0",
        width: "100vw",
        height: "100vh",
        backgroundColor: "rgba(0, 0, 0, 0.30)",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },

    container: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        gap: "16px",
        backgroundColor: "white",
        borderRadius: "10px",
        padding: "24px",
        width: "400px",
        boxSizing: "border-box",
    },

    headerBlock: {
        display: "flex",
        width: "100%",
        justifyContent: "space-between",
        alignItems: "center"
    },

    blockButtons: {
        display: "flex",
        gap: "8px",
    }


}
const buttonsStyle = createUseStyles(buttons);
const modalStyle = createUseStyles(modal);

export {buttonsStyle, modalStyle}