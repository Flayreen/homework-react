import React from 'react';
import '../styles/header.scss'
import logo from '../images/logo.svg'
import cart from '../images/cart.svg'
import favourite from '../images/favourite-header.svg'

import {Outlet, Link} from "react-router-dom";
import {useSelector} from "react-redux";

const Layout = (props) => {
    const {products} = useSelector(state => state.products);
    const countFavourites = products.filter(({isFavourite}) => isFavourite).length;
    const countCart = products.reduce((acc, {count}) => acc + count, 0);

    return (
        <>
            <header className="header">
                <Link to="/">
                    <img className="header__logo" src={logo} alt="logo"/>
                </Link>

                <div className="header__actions">
                    <Link to="/favourites">
                        <div className="header__actions__item">
                            <img src={favourite} alt="icon-favourite"/>
                            { countFavourites > 0 &&
                                (<div className="header__actions__item__counter">{countFavourites}</div>)
                            }
                        </div>
                    </Link>
                    <Link to="/cart">
                        <div className="header__actions__item">
                            <img src={cart} alt="icon-cart"/>
                            { countCart > 0 &&
                                (<div className="header__actions__item__counter">{countCart}</div>)
                            }
                        </div>
                    </Link>
                </div>
            </header>
            <Outlet/>
        </>
    );
};

Layout.propTypes = {

};

export default Layout;