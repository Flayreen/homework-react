import { createUseStyles } from 'react-jss';

import favourite from "../images/favourite.svg"
import favouriteHover from "../images/favourite-hover.svg"
import favouriteFilled from "../images/favourite-filled.svg"
const image = {
    img: {
        position: "absolute",
        top: "4px",
        right: "8px",
        content: `url(${favourite})`,
        cursor: "pointer",

        "&:hover": {
            content: `url(${favouriteHover})`,
        },
    },

    imgFilled: {
        position: "absolute",
        top: "4px",
        right: "8px",
        content: `url(${favouriteFilled})`,
        cursor: "pointer",
    }
}

const imageStyle = createUseStyles(image);

export {imageStyle}