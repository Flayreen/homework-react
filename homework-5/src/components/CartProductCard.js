import React from 'react';
import PropTypes from 'prop-types';
import '../styles/cartCards.scss'
import deleteIcon from '../images/delete.svg'
import {useDispatch, useSelector} from "react-redux";
import {increase, decrease} from "../redux/slices/productsSlice";
import {openModal} from "../redux/slices/modalSlice";

const CartProductCard = ({id, title, imgUrl, count, price}) => {
    const dispatch = useDispatch();

    // Отримання загальної суми одного товару
    const totalPrice = count * price;

    // Функції карточки в корзині
    function increaseItem() {
        dispatch(increase({id}))
    }
    function decreaseItem() {
        dispatch(decrease({id}))
    }
    function getModal() {
        dispatch(openModal({id}));
    }

    return (
        <div className="cart-card">
            <div className="cart-card__image-container">
                <img className="cart-card__image-container__image" src={imgUrl} alt="product photo"/>
            </div>
            <div className="cart-card__content">
                <div className="cart-card__content__header">
                    <span className="cart-card__content__header__title">{title}</span>
                    <button className="cart-card__content__header__delete" onClick={getModal}>
                        <img src={deleteIcon} alt="delete"/>
                    </button>
                </div>
                <div className="cart-card__content__actions">
                    <div className="cart-card__content__actions__counter">
                        <button className="cart-card__content__actions__counter__button" onClick={decreaseItem}>-</button>
                        <span className="cart-card__content__actions__counter__count">{count}</span>
                        <button className="cart-card__content__actions__counter__button" onClick={increaseItem}>+</button>
                    </div>
                    <span className="cart-card__content__actions__price">{totalPrice}$</span>
                </div>
            </div>
        </div>
    );
};

CartProductCard.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
};

CartProductCard.defaultProps = {
    title: "Product",
    imgUrl: "https://static.vecteezy.com/system/resources/previews/005/337/799/non_2x/icon-image-not-found-free-vector.jpg",
    count: 0,
}

export default CartProductCard;