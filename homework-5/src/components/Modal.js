import React from 'react';
import {modalStyle} from "../styles/style";
import PropTypes from "prop-types";
import closeIcon from "../images/close.svg"
import {closeModal} from "../redux/slices/modalSlice";
import {useDispatch} from "react-redux";

function Modal({header, closeButton, text, actions, closeFn}) {
    const modal = modalStyle();

    const dispatch = useDispatch();
    function removeModal() {
        dispatch(closeFn());
    }

    return (
        <div onClick={removeModal} className={modal.darkBackground}>
            <div onClick={(event) => event.stopPropagation()} className={modal.container}>
                <div className={modal.headerBlock}>
                    <h1 style={{fontSize: "24px", fontWeight: "700",}}>{header}</h1>
                    { closeButton && (
                        <img
                            src={closeIcon}
                            alt="icon-close"
                            onClick={removeModal}
                        />)}
                </div>
                <p>{text}</p>
                { Boolean(actions.length) && (
                    <div className={modal.blockButtons}>
                        {actions.map((ActionButton, index) => (
                            <ActionButton key={index}/>
                        ))}
                    </div>
                )}
            </div>
        </div>
    );
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.array.isRequired,
    closeButton: PropTypes.bool.isRequired,
    closeFn: PropTypes.func.isRequired,
};

Modal.defaultProps = {
    header: "Modal",
    text: "Please click blue button to continue",
    closeButton: true,
    closeFn(){},
}

export default Modal;