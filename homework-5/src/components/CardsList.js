import React, {useEffect} from 'react';
import ProductCard from "./ProductCard";
import "../styles/cards.scss"
import "../styles/loader.scss"

import {useDispatch, useSelector} from "react-redux";
import {fetchProducts, increase} from "../redux/slices/productsSlice";
import Modal from "./Modal";
import Button from "./Button";
import {closeModal} from "../redux/slices/modalSlice";


function CardsList(props) {
    const dispatch = useDispatch();
    const {products, status} = useSelector(state => state.products)

    useEffect(() => {
        dispatch(fetchProducts());
    }, []);

    const id = useSelector(state => state.modal.productId);
    const isOpenModal = useSelector(state => state.modal.isOpen);

    // Функції для модалки
    function removeModal() {
        dispatch(closeModal())
    }
    function addCart() {
        dispatch(increase({id}));
        dispatch(closeModal());
    }

    return (
        <>
            {status === "loading" && <div className="loader-container"><span className="loader"></span></div>}

            <div className="card-list">
                { products.length > 0 &&
                    products.map(({title, price, imgUrl, article, color, isFavourite, isCart}, index) => (
                        <ProductCard
                            id={index}
                            key={index}
                            title={title}
                            price={price}
                            imgUrl={imgUrl}
                            article={article}
                            color={color}
                        />
                    ))
                }
            </div>


            {isOpenModal && (
                <Modal
                    header="Add to card"
                    closeButton={true}
                    text="Do you realy want to add this item to cart?"
                    actions={[
                        () => <Button backgroundColor="blue" text="Add" fn={addCart}/>,
                        () => <Button backgroundColor="black" text="Cancel" fn={removeModal}/>
                    ]}
                    closeFn={closeModal}
                />
            )}
        </>
    );
}


export default CardsList;