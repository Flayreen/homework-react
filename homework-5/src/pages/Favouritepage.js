import React, {useState, useEffect} from 'react';
import '../styles/cards.scss'
import '../styles/favourite.scss'
import emptyImg from '../images/empty-page.jpg'

import ProductCard from "../components/ProductCard";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts, increase} from "../redux/slices/productsSlice";
import Modal from "../components/Modal";
import Button from "../components/Button";
import {closeModal} from "../redux/slices/modalSlice";

const Favouritepage = () => {
    const dispatch = useDispatch();

    // Отримання даних з сервака
    useEffect(() => {
        dispatch(fetchProducts());
    }, [])

    // Отмання усіх продуктів
    const allProducts = useSelector(state => state.products.products);

    // Modal
    const isModal = useSelector(state => state.modal.isOpen);
    const id = useSelector(state => state.modal.productId);

    function removeModal() {
        dispatch(closeModal())
    }
    function addCart() {
        dispatch(increase({id}));
        dispatch(closeModal());
    }

    return (
        <>
            <h1 className="page-title">Favourites</h1>
            {
                allProducts.filter(({isFavourite}) => isFavourite).length > 0
                    ? <div className="card-list favoirites-list">
                        {
                            allProducts.map(({title, price, imgUrl, article, color, isFavourite}, index) => {
                                if (isFavourite) {
                                    return (
                                        <ProductCard
                                            id={index}
                                            key={index}
                                            title={title}
                                            price={price}
                                            imgUrl={imgUrl}
                                            article={article}
                                            color={color}
                                        />
                                    )
                                }
                            })
                        }
                        {
                            // Модалка
                            isModal && (
                                <Modal
                                    header="Add to card"
                                    closeButton={true}
                                    text="Do you realy want to add this item to cart?"
                                    actions={[
                                        () => <Button backgroundColor="blue" text="Add" fn={addCart}/>,
                                        () => <Button backgroundColor="black" text="Cancel" fn={removeModal}/>
                                    ]}
                                    closeFn={closeModal}
                                />
                            )
                        }
                    </div>
                    : (<div className="not-found">
                        <img src={emptyImg} alt="empty image" width="300px"/>
                        <span>Empty page</span>
                    </div>)
            }
        </>
    );
};

export default Favouritepage;