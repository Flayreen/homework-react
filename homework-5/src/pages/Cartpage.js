import React, {useEffect, useState} from 'react';
import '../styles/cards.scss'
import '../styles/cartCards.scss'
import '../styles/form.scss'
import '../styles/favourite.scss'

import CartProductCard from "../components/CartProductCard";
import Modal from "../components/Modal";
import emptyImg from "../images/empty-page.jpg";
import Button from "../components/Button";
import PropTypes from "prop-types";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts, clear, clearCart} from "../redux/slices/productsSlice";
import {closeModal, toggleSuccessModal} from "../redux/slices/modalSlice";

import {Formik, Form, Field, ErrorMessage} from "formik";
import {order} from "../forms/order";
import {PatternFormat} from "react-number-format";


const Cartpage = () => {
    const dispatch = useDispatch();
    // Отримання даних з сервака
    useEffect(() => {
        dispatch(fetchProducts());
    }, [])

    // Отримання загальної суми усіх товарів в корзині
    const allProducts = useSelector(state => state.products.products);
    const totalPrice = allProducts.reduce((acc, {price, count}) => acc + (count * price), 0);

    //Функції модалки
    const isModal = useSelector(state => state.modal.isOpen);
    function removeModal() {
        dispatch(closeModal())
    }

    const id = useSelector(state => state.modal.productId);
    function removeItem() {
        dispatch(clear({id}));
        dispatch(closeModal());
    }

    // Form
    const initialFormValues = {
        name: "",
        surname: "",
        age: "",
        address: "",
        mobilePhone: "",
    }

    const isSuccessModal = useSelector(state => state.modal.isSuccessModal);
    function handleSubmit() {
        dispatch(toggleSuccessModal());
        dispatch(clearCart());
    }

    function clearAllCart() {
        dispatch(toggleSuccessModal());
    }



    return (
        <>
            <h1 className="page-title">Cart</h1>

            {
                allProducts.filter(({count}) => count > 0).length > 0
                    // Рендер карточок
                    ? <div className="cart-content-container">
                        <div className="cart-card-list">
                            {
                                allProducts.map(({title, imgUrl, count, price}, index) => {
                                    if (count > 0) {
                                        return (
                                            <CartProductCard
                                                id={index}
                                                key={index}
                                                title={title}
                                                imgUrl={imgUrl}
                                                count={count}
                                                price={price}
                                            />
                                        )
                                    }
                                })
                            }
                            {
                                // Модалка
                                isModal && (
                                    <Modal
                                        header="Remove item from cart"
                                        closeButton={true}
                                        text="Do you realy want to remove this item to cart?"
                                        actions={[
                                            () => <Button backgroundColor="#8d0000" text="Remove" fn={removeItem}/>,
                                            () => <Button backgroundColor="black" text="Cancel" fn={removeModal}/>
                                        ]}
                                        closeFn={closeModal}
                                    />
                                )
                            }
                        </div>

                        {/* ---- Форма ---- */}
                        <div className="form__body">
                            <div className="form-container">
                                <h2 className="form-container__title">Create order</h2>
                                <Formik initialValues={initialFormValues} validationSchema={order} onSubmit={handleSubmit}>
                                    {
                                        ({errors, touched, values}) => (
                                            <Form className="form">
                                                <div className="form__input-container">
                                                    <label htmlFor="name">Name</label>
                                                    <Field
                                                        className={errors.name && touched.name ? "form__input-container__input form__input-container__input--error" : "form__input-container__input"}
                                                        placeholder="Enter your name"
                                                        type="text"
                                                        name="name"
                                                        id="name"
                                                    />
                                                    <ErrorMessage className="form__input-container__error" name="name" component="p"/>
                                                </div>
                                                <div className="form__input-container">
                                                    <label htmlFor="surname">Surname</label>
                                                    <Field
                                                        className={errors.surname && touched.surname ? "form__input-container__input form__input-container__input--error" : "form__input-container__input"}
                                                        placeholder="Enter your surname"
                                                        type="text"
                                                        name="surname"
                                                        id="surname"
                                                    />
                                                    <ErrorMessage className="form__input-container__error" name="surname" component="p"/>
                                                </div>
                                                <div className="form__input-container">
                                                    <label htmlFor="age">Age</label>
                                                    <Field
                                                        className={errors.age && touched.age ? "form__input-container__input form__input-container__input--error" : "form__input-container__input"}
                                                        placeholder="Enter your age"
                                                        type="text"
                                                        name="age"
                                                        id="age"
                                                    />
                                                    <ErrorMessage className="form__input-container__error" name="age" component="p"/>
                                                </div>
                                                <div className="form__input-container">
                                                    <label htmlFor="address">Address</label>
                                                    <Field
                                                        className={errors.address && touched.address ? "form__input-container__input form__input-container__input--error" : "form__input-container__input"}
                                                        placeholder="Country, city"
                                                        type="text"
                                                        name="address"
                                                        id="address"
                                                    />
                                                    <ErrorMessage className="form__input-container__error" name="address" component="p"/>
                                                </div>
                                                <div className="form__input-container">
                                                    <label htmlFor="mobilePhone">Mobile phone</label>
                                                    <Field
                                                        className={errors.mobilePhone && touched.mobilePhone ? "form__input-container__input form__input-container__input--error" : "form__input-container__input"}
                                                        as={PatternFormat}
                                                        type="tel"
                                                        format="+38 (###)-###-##-##"
                                                        allowEmptyFormatting
                                                        mask="_"
                                                        name="mobilePhone"
                                                        id="mobilePhone"
                                                    />
                                                    <ErrorMessage className="form__input-container__error" name="mobilePhone" component="p"/>
                                                </div>

                                                <div className="form__payment-container">
                                                    <span className="form__payment-container__txt">To be paid:</span>
                                                    <span className="form__payment-container__total">{totalPrice}$</span>
                                                </div>

                                                <button className="form__button" type="submit">Order</button>
                                            </Form>
                                        )
                                    }
                                </Formik>
                            </div>
                        </div>
                    </div>


                    // Коли пуста корзина
                    : (<div className="not-found">
                        <img src={emptyImg} alt="empty image" width="300px"/>
                        <span>Empty page</span>
                    </div>)
            }

            {
                isSuccessModal &&
                <Modal
                    header="Success order"
                    closeButton={true}
                    text="You have successfuly created the order"
                    actions={[
                        () => <Button backgroundColor="blue" text="Done" fn={clearAllCart}/>,
                    ]}
                    closeFn={toggleSuccessModal}
                />
            }
        </>
    );
};

Cartpage.propTypes = {
    products: PropTypes.array.isRequired,
    fnDelete: PropTypes.func.isRequired,
    decreaseCart: PropTypes.func.isRequired,
    increaseCart: PropTypes.func.isRequired,
    total: PropTypes.number.isRequired,
    getTotalPrice: PropTypes.func.isRequired,
};

Cartpage.defaultProps = {
    products: [],
    fnDelete: () => {},
    decreaseCart: () => {},
    increaseCart: () => {},
    getTotalPrice: () => {},
    total: 0,
}


export default Cartpage;



