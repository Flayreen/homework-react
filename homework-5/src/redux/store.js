import {configureStore} from "@reduxjs/toolkit";
import productsReducer from './slices/productsSlice'
import modalReducer from './slices/modalSlice'

export default configureStore({
    reducer: {
        products: productsReducer,
        modal: modalReducer,
    }
})