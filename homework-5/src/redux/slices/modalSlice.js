import {createSlice} from "@reduxjs/toolkit";
// import {re}

const modalSlice = createSlice({
    name: "modal",
    initialState: {
        isOpen: false,
        isSuccessModal: false,
        productId: null,
    },
    reducers: {
        openModal(state, action) {
            state.isOpen = true;
            state.productId = action.payload.id;
        },

        closeModal(state) {
            state.isOpen = false;
            state.productId = null;
        },

        toggleSuccessModal(state, action) {
            state.isSuccessModal = !state.isSuccessModal;
        },

    }
})

export const {openModal, closeModal, toggleSuccessModal} = modalSlice.actions;
export default modalSlice.reducer;