import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";


export const fetchProducts = createAsyncThunk(
    "products/fetchProducts",
    async function(_, {rejectWithValue}) {
        try {
            const localProducts = localStorage.getItem("products");
            const parseLocalProducts = JSON.parse(localProducts);

            if (parseLocalProducts === null) {
                const response = await fetch("products.json");

                if (!response.ok) {
                    throw new Error("Products are not received!")
                }
                console.log(2)
                const data = await response.json();
                localStorage.setItem("products", JSON.stringify(data));
                return data;
            } else {
                return parseLocalProducts;
            }
        } catch (error) {
            return rejectWithValue(error.message);
        }
    }
)


const productsSlice = createSlice({
    name: "products",
    initialState: {
        products: [],
        status: null,
        error: null,
    },
    reducers: {
        toggleFavourite(state, action) {
            state.products[action.payload.id].isFavourite = !state.products[action.payload.id].isFavourite;
            localStorage.setItem("products", JSON.stringify(state.products));
        },

        increase(state, action) {
            state.products[action.payload.id].count++;
            localStorage.setItem("products", JSON.stringify(state.products));
        },

        decrease(state, action) {
            state.products[action.payload.id].count--;
            localStorage.setItem("products", JSON.stringify(state.products));
        },

        clear(state, action) {
            state.products[action.payload.id].count = 0;
            localStorage.setItem("products", JSON.stringify(state.products));
        },

        clearCart(state) {
            state.products = state.products.map(product => ({...product, count: 0}));
            localStorage.setItem("products", JSON.stringify(state.products));
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchProducts.pending, (state) => {
                state.status = "loading";
                state.error = "";
            })
            .addCase(fetchProducts.fulfilled, (state, action) => {
                state.status = "resolved";
                state.products = action.payload;
            })
            .addCase(fetchProducts.rejected, (state, action) => {
                state.status = "error";
                state.products = action.payload;
            })

    }
})

export const {toggleFavourite, increase, decrease, clear, clearCart} = productsSlice.actions;
export default productsSlice.reducer;