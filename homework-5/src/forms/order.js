import * as yup from "yup";

export const order = yup.object({
    name: yup
        .string()
        .matches(/^[^0-9]*$/, 'String should not contain any digits')
        .min(3, "Invalid name")
        .required("Please enter your name"),
    surname: yup
        .string()
        .matches(/^[^0-9]*$/, 'String should not contain any digits')
        .min(2, "Invalid surname")
        .required("Please enter your surname"),
    age: yup
        .string()
        .test('is-number', 'Please enter a valid number', (value) => {
            return !isNaN(parseInt(value, 10));
        })
        .min(2, "You're so young")
        .required("Please enter your age"),
    address: yup
        .string()
        .min(5, "Invalid address")
        .required("Please enter your address"),
    mobilePhone: yup
        .string()
        .matches(/^\+38 \(\d{3}\)-\d{3}-\d{2}-\d{2}$/, 'Invalid phone format')
        .min(6, "Invalid phone number")
        .required("Please enter your mobile phone"),
});

