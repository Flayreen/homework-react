import {configureStore} from "@reduxjs/toolkit";
import todoReducer from './slices/todoSlice'
import themeReducer from './slices/themeSlice'
import modalReducer from './slices/modalSlice'

export default configureStore({
    reducer: {
        todos: todoReducer,
        theme: themeReducer,
        modal: modalReducer,
    }
})