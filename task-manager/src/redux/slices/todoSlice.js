import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {closeModal} from "./modalSlice";

export const fetchTodos = createAsyncThunk(
    'todo/fetchTodos',
    async function(_, {rejectWithValue}) {
        try {
            const res = await fetch('https://jsonplaceholder.typicode.com/todos');

            if (!res.ok) {
                throw new Error("Server Error");
            }

            const data = await res.json();
            return data;

        } catch (error) {
            return rejectWithValue(error.message)
        }
    }
)

export const deleteTodo = createAsyncThunk(
    'todo/deleteTodo',
    async function(id, {rejectWithValue, dispatch}) {
        try {
            const response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
                method: 'DELETE'
            });


            if (!response.ok) {
                throw new Error("Error delete");
            }

            dispatch(removeTodo({id}));
            dispatch(closeModal());
        } catch (error) {
            return rejectWithValue(error.message)
        }

    }
)

export const postTodo = createAsyncThunk(
    'todo/postTodo',
    async function(taskInput, {rejectWithValue, dispatch, getState}) {
        try {
            const allTasks = getState().todos.todo;
            const body = {
                title: taskInput,
            }

            const res = await fetch('https://jsonplaceholder.typicode.com/todos', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body),
            });
            console.log(res)
            console.log(await res.text())

            if (!res.ok) {
                throw new Error("Error post");
            }

            dispatch(addTodo({body}));
        } catch (error) {
            return rejectWithValue(error.message);
        }
    }
)




const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todo: [],
        status: null,
        error: null,
    },
    reducers: {
        addTodo(state, action) {
            if (action.payload.body.title !== "") {
                state.todo.unshift(action.payload.body);
            }
        },
        removeTodo(state, action) {
            state.todo = state.todo.filter((_,index) => index !== action.payload.id);
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchTodos.pending, (state, action) => {
                state.status = "loading";
                state.error = "";
            })
            .addCase(fetchTodos.fulfilled, (state, action) => {
                state.status = "resolved";
                state.todo = action.payload;
            })
            .addCase(fetchTodos.rejected, (state, action) => {
                state.status = 'rejected';
                state.error = action.payload;
                console.log(action.payload)
            })
    }
})

export const {addTodo, removeTodo} = todoSlice.actions;
export default todoSlice.reducer;