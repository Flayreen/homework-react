import {createSlice} from "@reduxjs/toolkit";

const themeSlice = createSlice({
    name: 'theme',
    initialState: {
        theme: false
    },
    reducers: {
        changeTheme(state, action) {
            state.theme = !state.theme;
        }
    }
})

export const {changeTheme} = themeSlice.actions;
export default themeSlice.reducer;