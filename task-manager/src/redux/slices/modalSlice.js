import {createSlice} from "@reduxjs/toolkit";

const modalSlice = createSlice({
    name: 'modal',
    initialState: {
        isModal: false,
        postId: null,
    },
    reducers: {
        openModal(state, action) {
            state.isModal = !state.isModal;
            state.postId = action.payload.id;
        },

        closeModal(state, action) {
            state.isModal = !state.isModal;
            state.postId = null;
        }
    }
})

export const {openModal, closeModal} = modalSlice.actions;
export default modalSlice.reducer;