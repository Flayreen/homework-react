import './App.scss';
import Header from "./components/Header";
import TodoList from "./components/TodoList";
import {useSelector, useDispatch} from "react-redux";

function App() {
    const theme = useSelector(state => state.theme.theme);

  return (
    <div className={!theme ? "App" : "App App-dark"}>
        <Header />
        <TodoList />
    </div>
  );
}

export default App;
