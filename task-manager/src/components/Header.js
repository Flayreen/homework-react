import React, {useState} from 'react';
import '../App.scss'
import {useDispatch} from "react-redux";
import {postTodo} from "../redux/slices/todoSlice";
import {changeTheme} from "../redux/slices/themeSlice";

const Header = props => {
    const [taskInput, setTaskInput] = useState("");
    const dispatch = useDispatch();

    function addTaskOnEnter (event) {
        if (event.key === 'Enter') {
            addTask();
        }
    }

    function addTask() {
        console.log(taskInput)
        dispatch(postTodo(taskInput));
        setTaskInput("");
    }

    function toggleTheme() {
        dispatch(changeTheme())
    }

    return (
        <header className="header">
            <input
                className="header__input"
                type="text"
                value={taskInput}
                name="search"
                placeholder="Enter your task"
                onChange={(e) => setTaskInput(e.target.value)}
                onKeyDown={addTaskOnEnter}
            />

            <button className="header__button" onClick={addTask}>Add</button>
            <button className="header__button" onClick={toggleTheme}>Change theme</button>
        </header>
    );
};

export default Header;