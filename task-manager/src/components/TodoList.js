import React, {useEffect} from 'react';
import '../App.scss'
import Todo from './Todo'
import {useSelector, useDispatch} from "react-redux";
import {deleteTodo, fetchTodos} from "../redux/slices/todoSlice";
import {closeModal} from "../redux/slices/modalSlice";

const TodoList = props => {
    const dispatch = useDispatch();
    const {isModal, postId} = useSelector(state => state.modal);
    const {todo, status, error} = useSelector(state => state.todos);

    useEffect(() => {
        dispatch(fetchTodos());
    }, []);


    function removeTodo() {
        dispatch(deleteTodo(postId))
    }

    function exitModal(event) {
        dispatch(closeModal());
    }

    return (
        <div className="todo-list">

            {status === 'loading' && <h2>Loading</h2> }
            {status === 'rejected' && <h2>Error: {error}</h2> }

            {
                todo.map(({title}, index) => (
                    <Todo task={title} id={index} key={index}/>
                ) )
            }

            {isModal && (
                <div onClick={exitModal} className="modal-background">
                    <div onClick={(event) => event.stopPropagation()} className="modal">
                        <h2 className="modal__title">Remove task</h2>
                        <p className="modal__text">Do you really want to remove this task?</p>
                        <div className="modal__buttons-container">
                            <button onClick={removeTodo}>Remove</button>
                            <button onClick={exitModal}>Cancel</button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};


export default TodoList;