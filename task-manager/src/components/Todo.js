import React from 'react';
import '../App.scss'
import {useDispatch} from "react-redux";
import {openModal} from "../redux/slices/modalSlice";

const Todo = ({task, id}) => {
    const dispatch = useDispatch();

    function getModal() {
        dispatch(openModal({id}))
    }

    return (
        <>
            <div className="todo-list__todo">
                <span>{task}</span>
                <div>
                    <button onClick={getModal}>X</button>
                </div>
            </div>

        </>
    );
};


export default Todo;