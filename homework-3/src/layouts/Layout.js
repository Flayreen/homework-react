import React from 'react';
import '../styles/header.scss'
import logo from '../images/logo.svg'
import cart from '../images/cart.svg'
import favourite from '../images/favourite-header.svg'
import PropTypes from 'prop-types';
import {Outlet, Link} from "react-router-dom";

const Layout = ({countCart, countStar}) => {
    return (
        <>
            <header className="header">
                <Link to="/">
                    <img className="header__logo" src={logo} alt="logo"/>
                </Link>

                <div className="header__actions">
                    <Link to="/favourites">
                        <div className="header__actions__item">
                            <img src={favourite} alt="icon-favourite"/>
                            { countStar > 0 &&
                                (<div className="header__actions__item__counter">{countStar}</div>)
                            }
                        </div>
                    </Link>
                    <Link to="/cart">
                        <div className="header__actions__item">
                            <img src={cart} alt="icon-cart"/>
                            { countCart > 0 &&
                                (<div className="header__actions__item__counter">{countCart}</div>)
                            }
                        </div>
                    </Link>
                </div>
            </header>
            <Outlet/>
        </>
    );
};

Layout.propTypes = {

};

export default Layout;