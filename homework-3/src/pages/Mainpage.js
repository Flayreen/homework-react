import React from 'react';
import CardsList from "../components/CardsList";
import PropTypes from 'prop-types';

const Mainpage = props => {
    const {products, showModal, addFavourite} = props;
    return (
        <>
            <CardsList
                products={products}
                showModal={showModal}
                addFavourite={addFavourite}
            />
        </>
    );
};

Mainpage.propTypes = {

};

export default Mainpage;