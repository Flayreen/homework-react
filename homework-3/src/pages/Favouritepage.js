import React, {useState, useEffect} from 'react';
import '../styles/cards.scss'
import '../styles/favourite.scss'
import emptyImg from '../images/empty-page.jpg'

import ProductCard from "../components/ProductCard";

const Favouritepage = ({addFavourite, showModal, products}) => {

    const [favouriteCards, setFavouriteCards] = useState(products ? products : []);
    useEffect(() => {
        const local = JSON.parse(localStorage.getItem("products"));
        setFavouriteCards(local);
    }, [localStorage.getItem("products")]);

    return (
        <>
            <h1 className="page-title">Favourites</h1>
            {
                favouriteCards.filter(({isFavourite}) => isFavourite).length > 0
                    ? <div className="card-list favoirites-list">
                        {
                            favouriteCards.map(({title, price, imgUrl, article, color, isFavourite, isCart}, index) => {
                                if (isFavourite) {
                                    return (
                                        <ProductCard
                                            id={index}
                                            key={index}
                                            title={title}
                                            price={price}
                                            imgUrl={imgUrl}
                                            article={article}
                                            color={color}
                                            showModal={showModal}
                                            addFavourite={addFavourite}
                                            isFavourite={isFavourite}
                                        />
                                    )
                                }
                            })
                        }
                    </div>
                    : (<div className="not-found">
                        <img src={emptyImg} alt="empty image" width="300px"/>
                        <span>Empty page</span>
                    </div>)
            }
        </>
    );
};

Favouritepage.propTypes = {

};

export default Favouritepage;