import React, {useState, useEffect} from 'react';
import '../styles/cards.scss'
import '../styles/cartCards.scss'
import '../styles/favourite.scss'

import CartProductCard from "../components/CartProductCard";
import Modal from "../components/Modal";
import emptyImg from "../images/empty-page.jpg";
import Button from "../components/Button";
import PropTypes from "prop-types";


const Cartpage = ({products, fnDelete, decreaseCart, increaseCart, total, getTotalPrice}) => {
    function removeCart() {
        fnDelete(indexCard);
        openModal();
    }


    const [modal, setModal] = useState(false);
    const [indexCard, setIndexCard] = useState(null);
    const openModal = (index) => {
        if (!modal) {
            document.body.style.overflow = "hidden";
            setIndexCard(index);
        } else {
            document.body.style.overflow = "";
        }
        setModal(!modal);
    };

    const [cartProducts, setCartProducts] = useState(products ? products : []);
    useEffect(() => {
        const local = JSON.parse(localStorage.getItem("products"));
        setCartProducts(local);

        localStorage.setItem("products", JSON.stringify(products));
    }, [localStorage.getItem("products")]);

    return (
        <>
            <h1 className="page-title">Cart</h1>
            {
                cartProducts.filter(({count}) => count > 0).length > 0

                    // Рендер карточок
                    ? <div className="cart-card-list">
                        {
                            cartProducts.map(({title, imgUrl, article, color, count, isCart}, index) => {
                                if (count > 0) {
                                    return (
                                        <CartProductCard
                                            id={index}
                                            key={index}
                                            title={title}
                                            imgUrl={imgUrl}
                                            article={article}
                                            color={color}
                                            count={count}
                                            fnDelete={fnDelete}
                                            decreaseCart={decreaseCart}
                                            increaseCart={increaseCart}
                                            openModal={openModal}
                                            getTotalPrice={getTotalPrice}
                                        />
                                    )
                                }
                            })
                        }
                        {
                            // Модалка
                            modal && (
                                <Modal
                                    header="Remove item from cart"
                                    closeButton={true}
                                    text="Do you realy want to remove this item to cart?"
                                    actions={[
                                        () => <Button backgroundColor="#8d0000" text="Remove" fn={removeCart}/>,
                                        () => <Button backgroundColor="black" text="Cancel" fn={openModal}/>
                                    ]}
                                    closeFn={openModal}
                                />
                            )
                        }
                        <div className="total">
                            <div className="total__content">
                                <span className="total__content__caption">Total:</span>
                                <span className="total__content__price">{total}$</span>
                            </div>
                            <Button backgroundColor="blue" text="Buy"/>
                        </div>
                    </div>
                    : (<div className="not-found">
                        <img src={emptyImg} alt="empty image" width="300px"/>
                        <span>Empty page</span>
                    </div>)
            }
        </>
    );
};

Cartpage.propTypes = {
    products: PropTypes.array.isRequired,
    fnDelete: PropTypes.func.isRequired,
    decreaseCart: PropTypes.func.isRequired,
    increaseCart: PropTypes.func.isRequired,
    total: PropTypes.number.isRequired,
    getTotalPrice: PropTypes.func.isRequired,
};

Cartpage.defaultProps = {
    products: [],
    fnDelete: () => {},
    decreaseCart: () => {},
    increaseCart: () => {},
    getTotalPrice: () => {},
    total: 0,
}


export default Cartpage;