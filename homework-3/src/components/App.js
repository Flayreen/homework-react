import React, {useEffect, useState} from 'react';
import {Routes, Route} from "react-router-dom";

import Button from "./Button";
import Modal from "./Modal";
import Layout from "../layouts/Layout";
import Mainpage from "../pages/Mainpage";
import Favouritepage from "../pages/Favouritepage";
import Cartpage from "../pages/Cartpage";

function App() {
    // localStorage.removeItem("products")

    // Стан кількості зірок
    const [countFavorite, setCountFavourite] = useState(0);
    // Стан кількості в корзині
    const [countCart, setCountCart] = useState(0);

    // Отримання продуктів і збереження їх в local storage
    const localProducts = localStorage.getItem("products");
    const [products, setProducts] = useState(localProducts !== null ? JSON.parse(localProducts) : []);

    async function fetchProducts() {
        try {
            const response = await fetch("products.json");
            const data = await response.json();
            setProducts(data);
            localStorage.setItem("products", JSON.stringify(data));
        } catch (error) {
            console.error("Error fetching products:", error);
        }
    }

    useEffect(() => {
        if (localProducts === null) {
            fetchProducts();
        }
    }, []);



    // Функція добавлення у вибрані в хедері
    function addFavourite(isAdded, index) {
        const updatedProducts = [...products];
        updatedProducts[index].isFavourite = !isAdded;

        setProducts(updatedProducts);
    }

    // Хук стану модалки
    const [showModal, setShowModal] = useState(false);
    const [indexCard, setIndexCard] = useState(null);

    const openModal = (index) => {
        if (!showModal) {
            document.body.style.overflow = "hidden";
            setIndexCard(index);
        } else {
            document.body.style.overflow = "";
        }
        setShowModal(!showModal);
    };

    function addItem(index) {
        const updatedProducts = [...products];
        updatedProducts[index].count++;

        setProducts(updatedProducts);
        openModal()
    }

    // Операції з корзиною
    function deleteCart(index) {
        const updatedProducts = [...products];
        updatedProducts[index].count = 0;
        setProducts(updatedProducts);
    }

    function increaseCart(index) {
        const updatedProducts = [...products];
        updatedProducts[index].count++;
        setProducts(updatedProducts);
    }

    function decreaseCart(index) {
        const updatedProducts = [...products];
        updatedProducts[index].count--;
        setProducts(updatedProducts);
    }

    // Price for some card in cart
    function getTotalPrice(index) {
        const updatedProducts = [...products];
        const total = updatedProducts[index].count * updatedProducts[index].price;
        return total;
    }

    // Total price in CART
    const [total, setTotal] = useState(0);


    // Оновлення даних в залежності від масиву продуктів
    useEffect(() => {
        // Оновлення кількості зірок в хедері
        let countStar = products.filter(product => product.isFavourite).length;
        setCountFavourite(countStar);
        let countCart = products.reduce((accumulator, {count}) => accumulator + count, 0);
        setCountCart(countCart);
        let cardCart = products.reduce((accumulator, product) => accumulator + (product.count * product.price), 0);
        setTotal(cardCart);

        // Оновлення local storage
        localStorage.setItem("products", JSON.stringify(products));
    }, [products]);

    return (
      <div>
          <Routes>
              <Route path="/" element={<Layout countCart={countCart} countStar={countFavorite}/>}>
                  <Route path="/" element={<Mainpage products={products} showModal={openModal} addFavourite={addFavourite}/>}/>
                  <Route path="/favourites" element={<Favouritepage addFavourite={addFavourite} showModal={openModal} products={products}/>}/>
                  <Route path="/cart" element={<Cartpage products={products} fnDelete={deleteCart} increaseCart={increaseCart} decreaseCart={decreaseCart} total={total} getTotalPrice={getTotalPrice}/>}/>
              </Route>
          </Routes>

          {showModal && (
              <Modal
                  header="Add to card"
                  closeButton={true}
                  text="Do you realy want to add this item to cart?"
                  actions={[
                      () => <Button backgroundColor="blue" text="Add" fn={() => addItem(indexCard)}/>,
                      () => <Button backgroundColor="black" text="Cancel" fn={openModal}/>
                  ]}
                  closeFn={openModal}
              />
          )}
      </div>
    );
}

export default App;
