import React, {useState ,useEffect} from 'react';
import ProductCard from "./ProductCard";
import PropTypes from "prop-types";
import "../styles/cards.scss"

function CardsList({products, showModal, addFavourite}) {

    return (
        <div className="card-list">
            { products.length > 0 &&
                products.map(({title, price, imgUrl, article, color, isFavourite, isCart}, index) => (
                    <ProductCard
                        id={index}
                        key={index}
                        title={title}
                        price={price}
                        imgUrl={imgUrl}
                        article={article}
                        color={color}
                        showModal={showModal}
                        addFavourite={addFavourite}
                        isFavourite={isFavourite}
                    />
                ))
            }
        </div>
    );
}

CardsList.propTypes = {
    product: PropTypes.array.isRequired,
    showModal: PropTypes.func.isRequired,
    addFavourite: PropTypes.func.isRequired,
};

CardsList.defaultProps = {
  product: [],
  showModal() {},
  addFavourite(){},
}

export default CardsList;