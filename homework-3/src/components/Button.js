import React from 'react';
import {buttonsStyle} from "../styles/style";
import PropTypes from "prop-types";

function Button({text, backgroundColor, fn}) {
    const style = buttonsStyle();

    return (
        <button onClick={fn} className={style.button} style={{backgroundColor}}>
            {text}
        </button>
    );
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string,
    fn: PropTypes.func.isRequired,
};

Button.defaultProps = {
    backgroundColor: "black",
    fn(){},
}

export default Button;