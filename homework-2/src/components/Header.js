import React, {useState, useEffect} from 'react';
import '../styles/header.scss'
import logo from '../images/logo.svg'
import PropTypes from "prop-types";
import cart from '../images/cart.svg'
import favourite from '../images/favourite-header.svg'

function Header({countCart, countStar}) {

    return (
        <header className="header">
            <img className="header__logo" src={logo} alt="logo"/>

            <div className="header__actions">
                <div className="header__actions__item">
                    <img src={favourite} alt="icon-favourite"/>
                    { countStar > 0 &&
                        (<div className="header__actions__item__counter">{countStar}</div>)
                    }
                </div>
                <div className="header__actions__item">
                    <img src={cart} alt="icon-cart"/>
                    { countCart > 0 &&
                        (<div className="header__actions__item__counter">{countCart}</div>)
                    }
                </div>
            </div>
        </header>
    );
}

Header.propTypes = {
    countCart: PropTypes.number.isRequired,
    countStar: PropTypes.number.isRequired,
};

Header.defaultProps = {
    countCart: 0,
    countStar: 0,
}

export default Header;