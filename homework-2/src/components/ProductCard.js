import React, {useState} from 'react';
import "../styles/cards.scss"
import Button from "./Button";
import PropTypes from "prop-types";
import {imageStyle} from "../styles/image-style";

function ProductCard({id, title, price, imgUrl, article, color, showModal, addFavourite, isFavourite}) {
    const style = imageStyle();

    const [favourite, setFavourite] = useState(isFavourite);
    function addStar() {
        addFavourite(favourite, id);
        setFavourite(!favourite);
    }


    function getModal() {
        showModal(id);
    }
    return (
        <div className="card">
            <div className="card__image-block">
                <img className="card__image-block__image" src={imgUrl} alt="card-photo"/>
                <img className={favourite ? style.imgFilled : style.img}
                     alt="icon favourite"
                     onClick={addStar}
                />

            </div>
            <div className="card__content-block">
                <div className="card__content-block__header">
                    <h1 className="card__content-block__header__title">{title}</h1>
                    <span className="card__content-block__header__article">acticle: {article}</span>
                </div>
                <span className="card__content-block__color">Color: {color}</span>
                <div className="card__content-block__button-block">
                    <span className="card__content-block__button-block__price">{price}$</span>
                    <Button fn={getModal} backgroundColor="blue" text="Add to cart"/>
                </div>
            </div>
        </div>
    );
}

ProductCard.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    showModal: PropTypes.func.isRequired,
    addFavourite: PropTypes.func.isRequired,
    isFavourite: PropTypes.bool.isRequired,
}

ProductCard.defaultProps = {
    title: "Product title",
    price: "price",
    imgUrl: "https://static.vecteezy.com/system/resources/previews/005/337/799/non_2x/icon-image-not-found-free-vector.jpg",
    article: "not found",
    color: "not found",
    showModal(){},
    addFavourite(){},
    isFavourite: false,
}
export default ProductCard;