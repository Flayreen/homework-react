import React, {useEffect, useState} from 'react';
import Button from "./Button";
import Modal from "./Modal";
import CardsList from "./CardsList";
import Header from "./Header";

function App() {
    // localStorage.removeItem("products")

    // Стан кількості зірок
    const [countFavorite, setCountFavourite] = useState(0);
    // Стан кількості в корзині
    const [countCart, setCountCart] = useState(0);

    // Отримання продуктів і збереження їх в local storage
    const localProducts = localStorage.getItem("products");
    const [products, setProducts] = useState(localProducts !== null ? JSON.parse(localProducts) : []);

    async function fetchProducts() {
        try {
            const response = await fetch("products.json");
            const data = await response.json();
            setProducts(data);
            localStorage.setItem("products", JSON.stringify(data));
        } catch (error) {
            console.error("Error fetching products:", error);
        }
    }

    useEffect(() => {
        if (localProducts === null) {
            fetchProducts();
        }
    }, []);



    // Функція добавлення у вибрані в хедері
    function addFavourite(isAdded, index) {
        const updatedProducts = [...products];
        updatedProducts[index].isFavourite = !isAdded;

        setProducts(updatedProducts);
    }

    // Хук стану модалки
    const [showModal, setShowModal] = useState(false);
    const [indexCard, setIndexCard] = useState(null);

    const openModal = (index) => {
        if (!showModal) {
            document.body.style.overflow = "hidden";
            setIndexCard(index);
        } else {
            document.body.style.overflow = "";
        }
        setShowModal(!showModal);
    };

    function addItem(index) {
        const updatedProducts = [...products];
        updatedProducts[index].count++;

        setProducts(updatedProducts);
        openModal()
    }


    // Оновлення даних в залежності від масиву продуктів
    useEffect(() => {
        // Оновлення кількості зірок в хедері
        let countStar = products.filter(product => product.isFavourite).length;
        setCountFavourite(countStar);
        let countCart = products.reduce((accumulator, {count}) => accumulator + count, 0);
        setCountCart(countCart);

        // Оновлення local storage
        localStorage.setItem("products", JSON.stringify(products));
    }, [products]);

    return (
      <div>
          <Header
              countCart={countCart}
              countStar={countFavorite}
          />
          <CardsList
              products={products}
              showModal={openModal}
              addFavourite={addFavourite}
          />

          {showModal && (
              <Modal
                  header="Add to card"
                  closeButton={true}
                  text="Do you realy want to add this item to cart?"
                  actions={[
                      () => <Button backgroundColor="blue" text="Add" fn={() => addItem(indexCard)}/>,
                      () => <Button backgroundColor="black" text="Cancel" fn={openModal}/>
                  ]}
                  closeFn={openModal}
              />
          )}
      </div>
    );
}

export default App;
