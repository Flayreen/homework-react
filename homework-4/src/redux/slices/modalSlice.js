import {createSlice} from "@reduxjs/toolkit";
// import {re}

const modalSlice = createSlice({
    name: "modal",
    initialState: {
        isOpen: false,
        productId: null,
    },
    reducers: {
        openModal(state, action) {
            state.isOpen = !state.isOpen;
            state.productId = action.payload.id;
        },

        closeModal(state) {
            state.isOpen = !state.isOpen;
            state.productId = null;
        }
    }
})

export const {openModal, closeModal} = modalSlice.actions;
export default modalSlice.reducer;