import React from 'react';
import {Routes, Route} from "react-router-dom";

import Layout from "../layouts/Layout";
import Mainpage from "../pages/Mainpage";
import Favouritepage from "../pages/Favouritepage";
import Cartpage from "../pages/Cartpage";



function App() {
    // localStorage.removeItem("products");

    return (
      <div>
          <Routes>
              <Route path="/" element={<Layout/>}>
                  <Route path="/" element={<Mainpage/>}/>
                  <Route path="/favourites" element={<Favouritepage/>}/>
                  <Route path="/cart" element={<Cartpage/>}/>
              </Route>
          </Routes>
      </div>
    );
}

export default App;


