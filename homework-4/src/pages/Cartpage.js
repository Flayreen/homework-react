import React, {useState, useEffect} from 'react';
import '../styles/cards.scss'
import '../styles/cartCards.scss'
import '../styles/favourite.scss'

import CartProductCard from "../components/CartProductCard";
import Modal from "../components/Modal";
import emptyImg from "../images/empty-page.jpg";
import Button from "../components/Button";
import PropTypes from "prop-types";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts, clear} from "../redux/slices/productsSlice";
import {closeModal} from "../redux/slices/modalSlice";


const Cartpage = () => {
    const dispatch = useDispatch();
    // Отримання даних з сервака
    useEffect(() => {
        dispatch(fetchProducts());
    }, [])

    // Отримання загальної суми усіх товарів в корзині
    const allProducts = useSelector(state => state.products.products);
    const totalPrice = allProducts.reduce((acc, {price, count}) => acc + (count * price), 0);

    //Функції модалки
    const isModal = useSelector(state => state.modal.isOpen);
    function removeModal() {
        dispatch(closeModal())
    }

    const id = useSelector(state => state.modal.productId);
    function removeItem() {
        dispatch(clear({id}));
        dispatch(closeModal());
    }

    return (
        <>
            <h1 className="page-title">Cart</h1>
            {
                allProducts.filter(({count}) => count > 0).length > 0

                    // Рендер карточок
                    ? <div className="cart-card-list">
                        {
                            allProducts.map(({title, imgUrl, count, price}, index) => {
                                if (count > 0) {
                                    return (
                                        <CartProductCard
                                            id={index}
                                            key={index}
                                            title={title}
                                            imgUrl={imgUrl}
                                            count={count}
                                            price={price}
                                        />
                                    )
                                }
                            })
                        }
                        {
                            // Модалка
                            isModal && (
                                <Modal
                                    header="Remove item from cart"
                                    closeButton={true}
                                    text="Do you realy want to remove this item to cart?"
                                    actions={[
                                        () => <Button backgroundColor="#8d0000" text="Remove" fn={removeItem}/>,
                                        () => <Button backgroundColor="black" text="Cancel" fn={removeModal}/>
                                    ]}
                                />
                            )
                        }
                        <div className="total">
                            <div className="total__content">
                                <span className="total__content__caption">Total:</span>
                                <span className="total__content__price">{totalPrice}$</span>
                            </div>
                            <Button backgroundColor="blue" text="Buy"/>
                        </div>
                    </div>
                    : (<div className="not-found">
                        <img src={emptyImg} alt="empty image" width="300px"/>
                        <span>Empty page</span>
                    </div>)
            }
        </>
    );
};

Cartpage.propTypes = {
    products: PropTypes.array.isRequired,
    fnDelete: PropTypes.func.isRequired,
    decreaseCart: PropTypes.func.isRequired,
    increaseCart: PropTypes.func.isRequired,
    total: PropTypes.number.isRequired,
    getTotalPrice: PropTypes.func.isRequired,
};

Cartpage.defaultProps = {
    products: [],
    fnDelete: () => {},
    decreaseCart: () => {},
    increaseCart: () => {},
    getTotalPrice: () => {},
    total: 0,
}


export default Cartpage;