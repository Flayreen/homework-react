import React, {useState, useEffect} from 'react';
import '../styles/cards.scss'
import '../styles/favourite.scss'
import emptyImg from '../images/empty-page.jpg'

import ProductCard from "../components/ProductCard";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../redux/slices/productsSlice";

const Favouritepage = () => {
    const dispatch = useDispatch();

    // Отримання даних з сервака
    useEffect(() => {
        dispatch(fetchProducts());
    }, [])

    // Отмання усіх продуктів
    const allProducts = useSelector(state => state.products.products);

    return (
        <>
            <h1 className="page-title">Favourites</h1>
            {
                allProducts.filter(({isFavourite}) => isFavourite).length > 0
                    ? <div className="card-list favoirites-list">
                        {
                            allProducts.map(({title, price, imgUrl, article, color, isFavourite}, index) => {
                                if (isFavourite) {
                                    return (
                                        <ProductCard
                                            id={index}
                                            key={index}
                                            title={title}
                                            price={price}
                                            imgUrl={imgUrl}
                                            article={article}
                                            color={color}
                                        />
                                    )
                                }
                            })
                        }
                    </div>
                    : (<div className="not-found">
                        <img src={emptyImg} alt="empty image" width="300px"/>
                        <span>Empty page</span>
                    </div>)
            }
        </>
    );
};

export default Favouritepage;